from appium import webdriver

class Driver:

    def __init__(self):
        # Settings
        desired_caps = {
            "deviceName": "Google Pixel",
            "platformName": "Android",
            "appPackage": "com.firstgreatwestern.staging",
            "appActivity": "com.firstgroup.splash.controller.SplashActivity"
}

        self.instance = webdriver.Remote("http://0.0.0.0:4723/wd/hub", desired_caps)