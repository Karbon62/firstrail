from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class FirstRailScreen:
    def __init__(self, driver):
        self.driver = driver

        #--Ticket Tab--#
        self.root = WebDriverWait(self.driver.instance, 5).until(
            EC.visibility_of_element_located(
                MobileBy.ID, "com.firstgreatwestern.staging:id/action_bar_root"))

        #--Ticket search From/Start--#
        self.start = WebDriverWait(self.driver.instance, 5).until(
            EC.visibility_of_element_located(
                MobileBy.ID, "com.firstgreatwestern.staging:id/fromTextView"))

        #--Ticket search To--#
        self.to = WebDriverWait(self.driver.instance, 5).until(
            EC.visibility_of_element_located(
                MobileBy.ID, "com.firstgreatwestern.staging:id/toTextView"))

        #--Find Tickets Button--#
        self.find = WebDriverWait(self.driver.instance, 5).until(
            EC.visibility_of_element_located(
                MobileBy.ID, "com.firstgreatwestern.staging:id/searchTickets"))

        #--Ticket Search Result Screen--#
        self.tickets = WebDriverWait(self.driver.instance, 5).until(
            EC.visibility_of_element_located(
                MobileBy.ID, "com.firstgreatwestern.staging:id/serviceList"))

        #--Ticket Select--#
        self.select_tickets = WebDriverWait(self.driver.instance, 5).until(
            EC.visibility_of_element_located(
                MobileBy.ID, "com.firstgreatwestern.staging:id/selectionIndicator"))

        #--Ticket Selected--#
        self.tickets_selected = WebDriverWait(self.driver.instance, 5).until(
            EC.visibility_of_element_located(
                MobileBy.ID, "com.firstgreatwestern.staging:id/ticketsViewPager"))


        # Screen Interaction - Buttons, Clicks, Swipes

        def click_from_field(self):
            self.from_field.click()

        def select_default_from(self):
            self.station_name.click()

        def find_tickets(self):
            self.find_tickets_btn.click()

        def click_ticket_type(self):
            self.ticket_type.click()

