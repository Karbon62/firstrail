import unittest

from pageobject.pageobj import FirstRailScreen
from webdriver.driver import Driver


class LaunchApplication(unittest.TestCase):

    def setUp(self):
        self.driver = Driver()

    #--app launch - splash screen--#
    def test_launch_app(self):
        tickets = FirstRailScreen(self.driver)
        # App should take user directly to tickets tab by default

    #--ticket tab - Travel location--#
    def test_ticket_location(self):
        # From Field Insert
        insert = FieldSearch(self.driver)
        insert.click_fromfield
        # To Field Insert
        insert.click_tofield()

    #--ticket tab - select ticket type--#
    def test_ticket_type(self):
        #Select Ticket Type
        type = TicketType(self.dirver)
        type.click_singleticket()

    #--ticket tab - search tickets--#
    def test_search_tickets(self):
        #Search for tickets
        find = FindTicketsButton(self.driver)
        find.click_ticketsbutton()

    #--ticket search result screen - select ticket--#
    def test_ticket_select(self):
        #Select Ticket from results
        select = SelectTicket(self.driver)
        select.click_tickets()

    #--ticket tab - review tickets--#
    def test_review_ticket(self):
        #Review ticket select
        review = ReviewTicket(self.driver)
        review.click_tickets()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(FirstRailTicketsTestCases)
    unittest.TextTestRunner(verbosity=2).run(suite)